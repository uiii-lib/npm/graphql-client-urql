import { useCallback, useEffect, useState } from 'react';
import { getIntrospectionQuery } from 'graphql';
import { createClient as createWsClient } from 'graphql-ws';
import { cacheExchange, Client, createClient, dedupExchange, fetchExchange, subscriptionExchange } from 'urql';
import { devtoolsExchange } from '@urql/devtools';
import customScalarsExchange from 'urql-custom-scalars-exchange';

export interface UseCreateGrapqlClientOptions {
	graphqlUrl?: string;
	wsUrl?: string;
	scalars?: Record<string, (input: any) => any>
}

export const useCreateGraphqlClient = (options: UseCreateGrapqlClientOptions = {}) => {
	const {
		graphqlUrl = options.graphqlUrl || '/graphql',
		wsUrl = `${window.location.protocol.replace("http", "ws")}//${window.location.host}${graphqlUrl}`,
		scalars = {}
	} = options;

	const [client, setClient] = useState<Client>();

	const createUrqlClient = useCallback(async () => {
		const tmpClient = createClient({
			url: graphqlUrl
		});

		const result = await tmpClient.query(getIntrospectionQuery()).toPromise()
		const schema = result.data;

		const scalarsExchange = customScalarsExchange({
			schema,
			scalars: {
				DateTime(value) {
					return new Date(value);
				},
				...scalars
			},
		});

		const wsClient = createWsClient({
			url: wsUrl
		});

		const subsExchange = subscriptionExchange({
			forwardSubscription: (operation) => {console.log("fwd sub");return({
				subscribe: (sink) => {console.log("subs", operation, sink);return({
					unsubscribe: wsClient.subscribe(operation, sink)
				})}
			})}
		});

		const client = createClient({
			url: graphqlUrl!,
			exchanges: [devtoolsExchange, subsExchange, dedupExchange, scalarsExchange, cacheExchange, fetchExchange]
		});

		setClient(client);
	}, []);

	useEffect(() => {
		createUrqlClient();
	}, [createUrqlClient]);

	return client;
}
